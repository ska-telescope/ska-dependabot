# ska-dependabot

A repository used to run
[dependabot-gitlab](https://dependabot-gitlab.gitlab.io/dependabot/)
jobs using the
[dependabot-standalone](https://gitlab.com/dependabot-gitlab/dependabot-standalone)
CI template.

## Get Started

### Prepare the target repository

Create `.gitlab/dependabot.yml` with a
[Dependabot config](https://dependabot-gitlab.gitlab.io/dependabot/config/configuration.html).
Note that some options (notably `groups`, see
[this](https://gitlab.com/dependabot-gitlab/dependabot/-/issues/151) issue
for details) are
[unsupported](https://dependabot-gitlab.gitlab.io/dependabot/config/configuration.html#support-between-modes)
in GitLab.

### Create a scheduled pipeline

In this repository, create a
[scheduled pipeline](https://gitlab.com/ska-telescope/ska-dependabot/-/pipeline_schedules)
and fill out the following environment variables:

| VARIABLE            | value                                                    |
| ------------------- | -------------------------------------------------------- |
| PROJECT_PATH        | The target repository's project slug                     |
| DIRECTORY           | Corresponds with `directory` in `dependabot.yml`         |
| PACKAGE_MANAGER_SET | Corresponds with `package-ecosystem` in `dependabot.yml` |

For all available options (such as to use a configuration from a specific
branch) see
[here](https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html).

Choose a schedule to run the pipeline on, and save. When the pipeline runs
next (you may manually trigger it if you like), then dependabot will open
merge requests against the target repository according to the project's
`dependabot.yml` configuration.

## Example

As a simple example, consider the following configuration,
which would open new merge requests every day whenever anything in
`poetry.lock` can be updated that satisfies the dependencies in
`pyproject.toml`, or if there are newer dependencies
outside the versions constrained by `pyproject.toml`.

``` yaml
version: 2
updates:
  - package-ecosystem: pip
    directory: /
    schedule:
      interval: daily
```

This would need the below pipeline schedule:

![Example pipeline schedule](./img/example_scheduled_pipeline.png)
